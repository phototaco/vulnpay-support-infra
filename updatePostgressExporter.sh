#!/bin/bash

docker stop postgres_exporter;
docker rm postgres_exporter;

export POSTGRESDB=terraform-20240603221925089400000001.ccilvazcv0qe.us-west-2.rds.amazonaws.com

docker run -d \
  --name postgres_exporter \
  --network monitoring \
  -e DATA_SOURCE_NAME="postgresql://vulnpay:vulnpay1@${POSTGRESDB}:5432/postgres?sslmode=require" \
  -p 9187:9187 \
  quay.io/prometheuscommunity/postgres-exporter