#!/bin/bash

# Create Docker volumes (if they don't already exist)
docker volume create prometheus_data
docker volume create grafana_data

# Create a Docker network
docker network create monitoring

# Define environment variables
export VULNPAY1=${VULNPAY1:-10.0.2.100}
export VULNPAY2=${VULNPAY2:-10.0.2.101}
export VULNPAY3=${VULNPAY3:-10.0.2.102}
export VULNPAY4=${VULNPAY4:-10.0.2.103}
export POSTGRESDB=10.0.2.144

# Run Postgres Exporter container
docker run -d \
  --name postgres_exporter \
  --network monitoring \
  -e DATA_SOURCE_NAME="postgresql://vulnpay:vulnpay1@${POSTGRESDB}:5432/postgres?sslmode=disable" \
  -p 9187:9187 \
  quay.io/prometheuscommunity/postgres-exporter

# Wait for Postgres Exporter to be up
echo "Waiting for postgres_exporter to be available..."
until docker run --rm --network monitoring curlimages/curl:latest curl -s -f -o /dev/null http://postgres_exporter:9187/metrics; do
  echo "Waiting for postgres_exporter..."
  sleep 1
done
echo "postgres_exporter is up."

# Create Prometheus configuration file
cat << EOF > prometheus.yml
global:
  scrape_interval: 15s

rule_files:
  - 'tyk.rules.yml'

scrape_configs:
  - job_name: 'vulnpay'
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: 
        - '${VULNPAY1}:8082'
        - '${VULNPAY2}:8082'
        - '${VULNPAY3}:8082'
        - '${VULNPAY4}:8082'
    relabel_configs:
      - source_labels: [__address__]
        target_label: instance
  - job_name: 'vulnpay-tls'
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: 
        - '${VULNPAY1}:8082'
        - '${VULNPAY2}:8082'
        - '${VULNPAY3}:8082'
        - '${VULNPAY4}:8082'
    scheme: https
    tls_config:
      insecure_skip_verify: true
    relabel_configs:
      - source_labels: [__address__]
        target_label: instance
  - job_name: 'vulnpay-mtls'
    metrics_path: '/actuator/prometheus'
    static_configs:
      - targets: 
        - '${VULNPAY1}:8082'
        - '${VULNPAY2}:8082'
        - '${VULNPAY3}:8082'
        - '${VULNPAY4}:8082'
    scheme: https
    tls_config:
      cert_file: 'client.pem'
      key_file: 'client-key.pem'
      insecure_skip_verify: true
    relabel_configs:
      - source_labels: [__address__]
        target_label: instance
  - job_name: 'postgres'
    static_configs:
      - targets: ['postgres_exporter:9187','10.0.2.144:9100']
  - job_name: 'tyk'
    metrics_path: '/metrics'
    static_configs:
      - targets: 
        - '10.0.1.200:30998'
EOF

cat << EOF > tyk.rules.yml
groups:
  - name: tyk
    rules:
      - record: task:http_response_error_count
        expr: sum by (api) (tyk_http_status{code=~"5[0-9]{2}"}) or vector (0)

      - record: task:http_response_total_count
        expr: sum by (api) (tyk_http_status) 

      - record: task:http_response_error_rate
        expr: sum by (api) (rate(task:http_response_error_count[2m])) or vector (0)

  - name: slo_metrics
    rules:

      - record: job:slo_errors_per_request:ratio_rate2m
        expr: sum by (api) (rate(task:http_response_error_count[2m])) / sum by (api) (rate(task:http_response_total_count[2m]))

      - record: job:error_budget:remaining
        expr: (1 - job:slo_errors_per_request:ratio_rate2m) * 100
EOF

# Run Prometheus container
docker run -d \
  --name prometheus \
  --network monitoring \
  --user root \
  --mount type=bind,source=$(pwd)/prometheus.yml,target=/etc/prometheus/prometheus.yml \
  --mount type=bind,source=$(pwd)/client.pem,target=/etc/prometheus/client.pem \
  --mount type=bind,source=$(pwd)/client-key.pem,target=/etc/prometheus/client-key.pem \
  --mount type=bind,source=$(pwd)/tyk.rules.yml,target=/etc/prometheus/tyk.rules.yml \
  --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
  -p 9090:9090 \
  prom/prometheus:latest

# Run Grafana container
docker run -d \
  --name grafana \
  --network monitoring \
  --mount type=bind,source=$(pwd)/provisioning/datasources,target=/etc/grafana/provisioning/datasources \
  --mount type=bind,source=$(pwd)/provisioning/dashboards,target=/etc/grafana/provisioning/dashboards \
  --mount type=bind,source=$(pwd)/dashboards,target=/var/lib/grafana/dashboards \
  -e GF_SECURITY_ADMIN_PASSWORD=password \
  -e GF_LOG.CONSOLE=debug \
  -p 3000:3000 \
  grafana/grafana:latest
