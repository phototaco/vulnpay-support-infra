# Vulnpay Supporting Infrastructure
It is really hard to do any form of load testing without visibility into the runtime being tested. Grafan and prometheus are open source tools that provide good visiblity into the runtime. This repo provides a simple way to do that using docker-compose or the start.sh and stop.sh scripts.

## Requirements - Postgres
The postgres_exporter needs the ability to connect to the postgres database.  It needs a user (role in postgres terms) with the following permissions:
 - connect on postgres db
 - usage on public schema
 - create on public schema
 - execute on function pg_ls_waldir()

 The default is for a role name of ```vulnpay``` and password of ```vulnpay1```.  Update the start.sh and/or the docker-compose to change thoes values if needed.

Here is a script that grants the needed permissions to the vulnpay role:
```
GRANT CONNECT on DATABASE postgres to vulnpay;
GRANT USAGE on schema public to vulnpay;
GRANT CREATE on schema public to vulnpay;
GRANT EXECUTE ON FUNCTION pg_ls_waldir() TO vulnpay;
```

## Requirements - Docker Run
Besides docker being available, prometheus needs the IPs of the runtime to be specified in environment variables. Modify the start.sh script to set the environment variables to the IPs of the runtime where the Vulnpay application is running and the Postgres database is running.  Here are the defaults:
- VULNPAY1: 10.0.2.100
- VULNPAY2: 10.0.2.101
- VULNPAY3: 10.0.2.100
- POSTGRESDB: 10.0.2.141

## Requirements - Docker Compose
Besides docker-compose being available, prometheus needs the IPs of the runtime to be specified. This is done in two ways.

1.  In the prometheus-compose.yaml as a list of IPs rather than using environment variables. The defaults are:
- 192.168.1.3 (vulnpay1)
- 192.168.1.6 (vulnpay2)

2. The postgres DB hostname is specified by the system environment variable ```POSTGRESDB```. There is no default.