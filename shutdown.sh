#!/bin/bash

docker stop $(docker ps -q) && docker rm $(docker ps -a -q)

docker network rm monitoring

docker volume rm prometheus_data
docker volume rm grafana_data